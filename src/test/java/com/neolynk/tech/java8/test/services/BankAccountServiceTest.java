package com.neolynk.tech.java8.test.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.neolynk.tech.java8.model.BankAccount;
import com.neolynk.tech.java8.model.BankUser;
import com.neolynk.tech.java8.services.impl.BankingServiceImpl;

public class BankAccountServiceTest {
	
	private BankingServiceImpl service;
	String firstName = "Jack";
	String lastName = "long";
	String address = "30 rue du four 75005 Paris";
	String phone = "06969493939";
	int age = 23;

	
	@Before
	public void setUp() throws Exception {
		service = new BankingServiceImpl();
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}

	@Test
	public void testAddUserAccountBankUserString() {
		BankUser user = service.addUser(firstName, lastName, address, phone, age);
		service.addUser(user );
		UUID accountID = service.addUserAccount(user, "Account1");
		Optional<BankAccount> found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  0.0);
		
	}

	@Test
	public void testAddUserAccountBankUserStringDouble() {
		BankUser user = service.addUser(firstName, lastName, address, phone, age);
		service.addUser(user );
		double balance = 1000.0;
		UUID accountID = service.addUserAccount(user, "Account1", balance);
		Optional<BankAccount> found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
	}

	@Test
	public void testAddUserAccountByUserIDUUIDString() {
		BankUser user = service.addUser(firstName, lastName, address, phone, age);
		service.addUser(user );
		double balance = 0.0;
		UUID accountID = service.addUserAccountByUserID(user.getId(), "Account1");
		Optional<BankAccount> found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
	}

	@Test
	public void testAddUserAccountByUserIDUUIDStringDouble() {
		BankUser user = service.addUser(firstName, lastName, address, phone, age);
		service.addUser(user );
		double balance = 1000.0;
		UUID accountID = service.addUserAccountByUserID(user.getId(), "Account1", balance);
		Optional<BankAccount> found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
	}

	@Test
	public void testCreditUserAccountAccount() {
		BankUser user = service.addUser(firstName, lastName, address, phone, age);
		service.addUser(user );
		double balance = 0.0;
		UUID accountID = service.addUserAccountByUserID(user.getId(), "Account1");
		Optional<BankAccount> found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
		balance = 100.0;
		service.creditUserAccountAccount(accountID, 100.0);
		found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
	}

	@Test
	public void testDebitUserAccountAccount() {
		BankUser user = service.addUser(firstName, lastName, address, phone, age);
		service.addUser(user );
		double balance = 0.0;
		UUID accountID = service.addUserAccountByUserID(user.getId(), "Account1");
		Optional<BankAccount> found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
		balance = 100.0;
		service.debitUserAccountAccount(user, accountID, 100.0);
		found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  -balance);
	}

	@Test
	public void testFindBankAccountById() {
		BankUser user = service.addUser(firstName, lastName, address, phone, age);
		service.addUser(user );
		double balance = 0.0;
		UUID accountID = service.addUserAccountByUserID(user.getId(), "Account1");
		Optional<BankAccount> found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
		balance = 100.0;
		service.creditUserAccountAccount(accountID, 100.0);
		found = service.findBankAccountById(accountID);
		assertTrue("Bank Account not created", found.isPresent());
		assertEquals("Account1", found.get().getName());
		assertTrue("Wrong Account balance ", found.get().getBalance() ==  balance);
	}

}

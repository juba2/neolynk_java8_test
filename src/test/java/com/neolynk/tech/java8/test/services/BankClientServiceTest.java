package com.neolynk.tech.java8.test.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Optional;
import java.util.OptionalDouble;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.neolynk.tech.java8.model.BankAccount;
import com.neolynk.tech.java8.model.BankUser;
import com.neolynk.tech.java8.services.BankClientService;
import com.neolynk.tech.java8.services.impl.BankingServiceImpl;

public class BankClientServiceTest {

	private BankClientService service;
	String firstName = "Jack";
	String lastName = "long";
	String address = "30 rue du four 75005 Paris";
	String phone = "06969493939";
	int age = 23;

	@Before
	public void setUp() throws Exception {
		service = new BankingServiceImpl();
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}

	@Test
	public void testAddUserStringStringStringStringInt() {

		BankUser user = service.addUser(firstName, lastName, address, phone, age);

		checkUser(user, age, firstName, lastName, address, phone);

	}

	private void checkUser(BankUser userToCheck, int age, String fName, String lName, String Addr, String phone) {
		assertTrue("Error user is null",  userToCheck != null);
		assertTrue("Error user id null",  userToCheck.getId() != null);
		assertTrue("Error user first name is null",  userToCheck.getFirstName() != null);
		assertEquals(fName, userToCheck.getFirstName());
		assertTrue("Error user last name is null",  userToCheck.getLastName() != null);
		assertEquals(lName, userToCheck.getLastName());
		assertTrue("Error user address is null",  userToCheck.getAddress() != null);
		assertEquals(Addr, userToCheck.getAddress());
		assertTrue("Error user age not " + age,  userToCheck.getAge() ==  age);
	}

	@Test
	public void testAddUserBankUser() {

		BankUser user = new BankUser(firstName, lastName, address, phone, age);
		user = service.addUser(user);
		checkUser(user, age, firstName, lastName, address, phone);
	}

	@Test
	public void testUpdateUser() {
		// create user
		BankUser user  = service.addUser(firstName, lastName, address, phone, age);

		user.setAge(32);
		// update
		service.updateUser(user);

		Optional<BankUser> found = service.findUserById(user.getId());
		assertTrue("Error user not found", found.isPresent());
		assertTrue("Age not update", user.getAge() == found.get().getAge());


	}

	@Test
	public void testDeleteUserById() {
		// create user
		BankUser user  = service.addUser(firstName, lastName, address, phone, age);
		
		// delete user
		service.deleteUserById(user.getId());
		Optional<BankUser> found = service.findUserById(user.getId());
		assertFalse("Error deleted user found", found.isPresent());

	}

	@Test
	public void testDeleteUser() {
		// create user
		BankUser user  = service.addUser(firstName, lastName, address, phone, age);

		// delete user
		service.deleteUser(user);
		Optional<BankUser> found = service.findUserById(user.getId());
		assertFalse("Error deleted user found", found.isPresent());
	}

	@Test
	public void testGetAllUsers() {
		int max = 10;
		// create user
		for (int i = 0 ; i < max  ; i++) {
			service.addUser(firstName+"_#"+i, lastName+"_#"+i, address+"_#"+i, phone+"_#"+i, age+i);
		}
	
		Collection<BankUser> users = service.getAllUsers();
		assertTrue("Users are null", users != null);
		
		assertTrue("Users count error", users.size() == max);
		
	}

	@Test
	public void testFindUserById() {
		// create user
		BankUser user  = service.addUser(firstName, lastName, address, phone, age);
		Optional<BankUser> found = service.findUserById(user.getId());
		assertTrue("Error user not found", found.isPresent());
	}

	@Test
	public void testGetUserByFistNameAndLastName() {
		// create user
		BankUser user  = service.addUser(firstName, lastName, address, phone, age);
		Optional<BankUser> found = service.getUserByFistNameAndLastName(firstName, lastName);
		assertTrue("Error user not found", found.isPresent());
		assertEquals(firstName, found.get().getFirstName());
		assertEquals(lastName, found.get().getLastName());
	}

	@Test
	public void testGetUserAccounts() {
		// create user
		BankUser user  = service.addUser(firstName, lastName, address, phone, age);
		BankAccount acc1 = new BankAccount("acc1");
		BankAccount acc2 = new BankAccount("acc2");
		user.getAccounts().add(acc1);
		user.getAccounts().add(acc2);
		Collection<BankAccount> accounts = service.getUserAccounts(user.getId());
		
		assertTrue("accounts are null", accounts != null);
		assertTrue("accounts count error", accounts.size() == 2);	
	}

	@Test
	public void testAccountBalanceByUserID() {
		BankUser user  = service.addUser(firstName, lastName, address, phone, age);
		BankAccount acc1 = new BankAccount("acc1", 22.0);
		BankAccount acc2 = new BankAccount("acc2", 3000.0);
		user.getAccounts().add(acc1);
		user.getAccounts().add(acc2);
		OptionalDouble balance = service.accountBalanceByUserID(user.getId());
		
		assertTrue("balance not present", balance.isPresent());
		assertTrue("balance not correct", balance.getAsDouble() == (acc1.getBalance() + acc2.getBalance()));
	}

}

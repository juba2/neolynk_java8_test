package com.neolynk.tech.java8.services;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.UUID;

import com.neolynk.tech.java8.model.BankAccount;
import com.neolynk.tech.java8.model.BankUser;

public interface BankClientService {

	
	/*
	 * Add new User
	 */
	public BankUser addUser(String firstName, String lastName, String address, String phone, int age);
	
	/**
	 * Add a new user
	 * @param user
	 * @return
	 */
	public BankUser addUser(BankUser user);
	
	/**
	 * Update user
	 * @param user
	 */
	public void updateUser(BankUser user);
	
	
	
	/**
	 * Delete User by ID
	 * @param userID
	 */
	public void deleteUserById(UUID  userID);
	
	/**
	 * Delete User
	 * @param user
	 */
	public default void deleteUser(BankUser user) {
		if (user == null) {
			throw new IllegalArgumentException("The user to delete must not be null");
		}
		deleteUserById(user.getId());
	}
	
	/**
	 * Get All users
	 * @return
	 */
	public Collection<BankUser> getAllUsers();
	
	
	/**
	 * Get Bank Account by UUID
	 * @param id
	 * @return
	 */
	public default  Optional<BankUser>  findUserById(UUID id) {
		if (id == null) {
			throw new IllegalArgumentException("The id must not be null");
		}
		return getAllUsers().stream().filter(e->e.getId().equals(id)).findFirst();
	}
	
	/**
	 * Get Bank Account by UUID
	 * @param id
	 * @return
	 */
	public default  Optional<BankUser>  getUserByFistNameAndLastName(String firstName, String lastName) {
		if (firstName == null || lastName == null) {
			throw new IllegalArgumentException("The first name or last name must not be null");
		}
		return getAllUsers().stream().filter(e->e.getFirstName().equals(firstName) && e.getLastName().equals(lastName)).findFirst();
	}
	
	public default  Collection<BankAccount> getUserAccounts(UUID id) {
		 Optional<BankUser> optionalUser = findUserById(id);
		if (optionalUser.isPresent()) {
			return optionalUser.get().getAccounts();
		}
		return Collections.emptyList();
	}
	
	
	
	public default OptionalDouble accountBalanceByUserID(UUID id) {
		Collection<BankAccount> userAccounts = getUserAccounts(id);
		
		if (userAccounts.isEmpty()) {
			return OptionalDouble.of(0);
		}
		return OptionalDouble.of(userAccounts.stream().mapToDouble(BankAccount::getBalance).sum());
	}
	
	
	
	
}

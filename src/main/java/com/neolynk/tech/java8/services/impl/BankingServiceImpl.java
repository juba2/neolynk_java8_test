package com.neolynk.tech.java8.services.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.neolynk.tech.java8.model.BankAccount;
import com.neolynk.tech.java8.model.BankUser;
import com.neolynk.tech.java8.services.BankAccountService;
import com.neolynk.tech.java8.services.BankClientService;

public class BankingServiceImpl implements BankAccountService, BankClientService {

	private Map<UUID, BankUser> usersList = new HashMap<>();
	private Map<UUID, BankAccount> bankAccounts = new HashMap<>();  
	
	@Override
	public BankUser addUser(String firstName, String lastName, String address, String phone, int age) {
		BankUser user = new BankUser(firstName, lastName, address, phone, age);
		usersList.put(user.getId(), user);
		return user;
	}

	@Override
	public void deleteUserById(UUID userID) {
		BankUser bankUser = usersList.remove(userID);
		if (bankUser != null) {
			for (BankAccount acc  : bankUser.getAccounts()) {
				bankAccounts.remove(acc.getAccountId());
			}
		}
	}

	@Override
	public Collection<BankUser> getAllUsers() {
		return usersList.values();
	}

	@Override
	public BankUser addUser(BankUser user) {
		usersList.put(user.getId(), user);
		return user;
	}

	@Override
	public void updateUser(BankUser user) {
		usersList.put(user.getId(), user);
	}

	@Override
	public UUID addUserAccount(BankUser user, String accountName, double balance) {
		BankAccount account = new BankAccount(accountName, balance);
		user.getAccounts().add(account);
		bankAccounts.put(account.getAccountId(), account);
		return account.getAccountId();
	}

	@Override
	public UUID addUserAccountByUserID(UUID userId, String accountName, double balance) {
		Optional<BankUser> optionalUser = findUserById(userId);
		if (optionalUser.isPresent()) {
			return addUserAccount(optionalUser.get(), accountName, balance);			
		}
		throw new IllegalArgumentException("User not found with the given id" + userId);
	}


	@Override
	public Optional<BankAccount> findBankAccountById(UUID accountId) {
		return Optional.ofNullable(bankAccounts.get(accountId));
	}

}

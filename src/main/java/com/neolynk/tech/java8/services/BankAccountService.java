package com.neolynk.tech.java8.services;

import java.util.Optional;
import java.util.UUID;

import com.neolynk.tech.java8.model.BankAccount;
import com.neolynk.tech.java8.model.BankUser;

public interface BankAccountService {

	/**
	 * Add  user Account
	 * @param user
	 * @param accountName
	 * @return Account Id
	 */
	public default UUID addUserAccount(BankUser user,  String accountName) {
		return addUserAccount(user, accountName, 0.0);
	}
	
	/**
	 * Add user Account with initial balance
	 * @param user
	 * @param accountName
	 * @param balance : initial balance
	 */
	public UUID addUserAccount(BankUser user, String accountName, double balance);
	
	
	/**
	 * Add  user Account
	 * @param user
	 * @param accountName
	 * @return Account Id
	 */
	public default UUID addUserAccountByUserID(UUID userId,  String accountName)  {
		return addUserAccountByUserID(userId, accountName, 0.0);
	}
	
	
	/**
	 * Add user Account with initial balance
	 * @param user
	 * @param accountName
	 * @param balance : initial balance
	 */
	public UUID addUserAccountByUserID(UUID userId, String accountName, double balance);
	
	
	/**
	 * Credit account
	 * @param accountID
	 * @param amount : must be positive double
	 */
	public default void creditUserAccountAccount(UUID accountID, double amount) {
		if (amount < 0) {
			throw new IllegalArgumentException("amount must not be negative");
		}
	   Optional<BankAccount> optionalAccount = findBankAccountById(accountID);
	   
	   if (optionalAccount.isPresent()) {
		   BankAccount account = optionalAccount.get();
		   account.setBalance(account.getBalance() + amount);
	   } else {
		   throw new IllegalArgumentException("Account not found with" + accountID);
	   }
	}
	
	/**
	 * Debit account 
	 * @param accountID
	 * @param amount : to debit must be positive double
	 */
	public default void debitUserAccountAccount(BankUser user, UUID accountID, double amount) {
		if (amount < 0) {
			throw new IllegalArgumentException("amount must not be negative");
		}
	   Optional<BankAccount> optionalAccount = findBankAccountById(accountID);
	   
	   if (optionalAccount.isPresent()) {
		   BankAccount account = optionalAccount.get();
		   account.setBalance(account.getBalance() - amount);
	   } else {
		   throw new IllegalArgumentException("Account not found with" + accountID);
	   }
	}
	
	/**
	 * 
	 * @param accountId
	 * @return
	 */
	public Optional<BankAccount> findBankAccountById(UUID accountId);
	
}

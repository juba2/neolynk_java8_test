package com.neolynk.tech.java8.model;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Banking Account
 * @author Juba2
 */
public class BankAccount {
	
	/**
	 * Account ID
	 */
	private UUID accountId;
	
	/**
	 * 
	 */
	private String name;
	/**
	 * Account Balance
	 */
	private double balance;
	/**
	 * Creation date
	 */
	private LocalDate creationDate;
	
	public BankAccount(String name) {
		this(name, 0.0);
	
	}

	public BankAccount(String name, double balance) {
		super();
		this.balance = balance;
		this.accountId = UUID.randomUUID();
		this.creationDate = LocalDate.now();
		this.name = name;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	/**
	 * Get the Account Unique Identifier
	 * @return
	 */
	public UUID getAccountId() {
		return accountId;
	}


	/**
	 * Get Account Balance
	 * @return
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * Set The BAnking Account balance 
	 * @param balance
	 */
	
	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BankAccount [accountId=" + accountId + ", name=" + name + ", balance=" + balance + ", creationDate="
				+ creationDate + "]";
	}

	
	
}
